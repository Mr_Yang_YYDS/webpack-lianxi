import Vue from 'vue'
import VueRouter from 'vue-router';
import Router from 'vue-router'

Vue.use(Router);

const routes = [
    {
        path:'/',
        redirect:'index'
    },
    {
        path:'/index',
        name:'index',
        component: () => import('../views/index/index.vue')
    },
    {
        path:'/classify',
        name:'classify',
        redirect:'/classify/fruits',//重定向
        component: () => import('../views/classify/index.vue'),
        children:[
            {
                path:'/classify/fruits',
                name:'fruits',
                component: () => import('../views/classify/module/fruits')
            },
            {
                path:'/classify/vegetables',
                name:'vegetables',
                component: () => import('../views/classify/module/vegetables')
            },
        ]
    },
    {
        path:'/selected',
        name:'selected',
        component: () => import('../views/selected/index.vue')
    },
    {
        path:'/cart',
        name:'cart',
        component: () => import('../views/cart/index.vue')
    },
    {
        path:'/my',
        name:'my',
        component: () => import('../views/my/index.vue')
    },
    {
        path:'/nav',
        name:'nav',
        component: () => import('../views/nav.vue')
    },
    {
        path:'/details',
        name:'details',
        component: () => import('../views/index/details.vue')
    },
]
const router =new VueRouter({
    routes,
    //路由跳转时动态添加样式
    linkActiveClass:'selected'
})
export default router