// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Vuex from 'vuex'
import VueRouter from 'vue-router';
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import store from './store';
import '@/assets/css/common.css'
import '@/assets/css/index.css'
//引入字体图标库
import '@/assets/font/iconfont.css'

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(ElementUI);

Vue.config.productionTip = false

// /* eslint-disable no-new */
// new Vue({
//   el: '#app',
//   //使用router配置
//   router,
//   //store,
//   render: h => h(App)
// })

new Vue({
  router,
  store,
  render: h=> h(App)
}).$mount('#app')
