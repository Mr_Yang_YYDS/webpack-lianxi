import Vue from 'vue'
import Router from 'vue-router'
import Login from '../views/Login'
import Main from '../views/Main'
import MemberList from '../views/Member/MemberList'
import MemberLeve from '../views/Member/MemberLeve'
import { name } from 'file-loader'
import NotFound from '../views/NotFound'
//下面是练习
import One from '../lianxi/One'
import Second from '../lianxi/Second'
import Three from '../lianxi/Three'
import Four from '../lianxi/Four'
import Five from '../lianxi/Five'
import Six from '../lianxi/Six'
import Seven from '../lianxi/Seven'
import Eight from '../lianxi/Eight'
import Kehu from '../Xindai/Kehu'
import Nine from '../lianxi/Nine'
import Shouye from '../views/start/Shouye'
import Caidan from '../views/start/Caidan'

Vue.use(Router);

export default new Router({
  mode:'history',
  routes:[
  {
    //登录页
    path:'/login',
    name:'Login',
    component:Login
  },
  {
    //首页
    path:'/main/:name',
    name:'Main',
    component:Main,
    children:[
        {
            path:'/member/list/:id',
            name:'MemberList',
            component:MemberList,
            props:true
        },
        {
            path:'/member/leve',
            name:'MemberLeve',
            component:MemberLeve
        },
    ]
  },
  {
    path:'/goMain/:name',
    redirect:'/main/:name'
  },
  {
    path:'/one',
    name:'One',
    component:One
  },
  {
    path:'/second',
    name:'Second',
    component:Second
  },
  {
    path:'/three',
    name:'Three',
    component:Three
  },
  {
    path:'/four',
    name:'Four',
    component:Four
  },
  {
    path:'/five',
    name:'Five',
    component:Five
  },
  {
    path:'/six',
    name:'Six',
    component:Six
  },
  {
    path:'/seven',
    name:'Seven',
    component:Seven
  },
  {
    path:'/eight',
    name:'Eight',
    component:Eight
  },
  {
    path:'/kehu',
    name:'Kehu',
    component:Kehu
  },
  {
    path:'/nine',
    name:'Nine',
    component:Nine
  },
  {
    path:'/caidan',
    name:'Caidan',
    component:Caidan
  },
  {
    path:'/shouye',
    name:'Shouye',
    component:Shouye
  },
  {
    path:'*',
    component:NotFound
  }
]
})