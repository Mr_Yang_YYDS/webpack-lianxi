/*
import Vue from 'vue'
import  Vuex from 'vuex'

Vue.use(Vuex);

//全局state对象，用于保存所有组件的公共数据
const state = {
    user:{
        name:''
    }
};

//监听state对象的知道额最新状态
const getters={
    getUser(state){
        return state.user;
    }
};

//唯一一个可以修改state值得方法(同步执行)
const mutations ={
    updateUser(state,user){
        state.user = user;
    }
};

//异步执行mutation方法
const actions ={
    asyncUpdateUser(context,user){
        context.commit("updateUser",user);
    }
};

export default new Vuex.Store({
    state,
    getters,
    mutations,
    actions
})*/