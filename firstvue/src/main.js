import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Vuex from 'vuex'
//import store from './store'

Vue.use(VueRouter);
Vue.use(ElementUI);
Vue.use(Vuex);

Vue.config.productionTip = false

//路由跳转之前
router.beforeEach((to,from,next)=>{
  let isLogin = sessionStorage.getItem('isLogin');
  //注销登录
  if(to.path == '/logout'){
    //清空账号存储
    sessionStorage.clear();
    //注销后跳转到登录页面
    next({path:'login'});
  }else if(to.path == '/login'){
    //判断是否为空，不为空跳转到首页
    if(isLogin != null){
      next({path:'/main'});
    }
    //如果为空跳到登陆页面
  }else if(isLogin == null){
    next({path:'/login'});
  }
  //跳到钩子函数那边
  next();
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  //使用router配置
  router,
  //store,
  render: h => h(App)
})
